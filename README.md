# Cross-platform C development for MSX on MacOS

## Goal

The goal of this repository (and these pages for that matter) is to provide the means to start developing for MSX on MacOS in an easy way. Installation scripts and tools are provides to setup a development environment and example code is provided to get you started.

## Host and target platforms

The target platform is the Z80-based MSX (see http://nl.wikipedia.org/wiki/MSX), but a lot of the tools and information on this site can be applied to other target platforms (eg. other Z80-based retro computers like Amstrad, Coleco, Memotech, Radio Shack TRS-80 model 1 but also other processors supported by the SDCC compiler).

The target platform is assumed to run MSX-DOS 2 or a compatible DOS (like [NEXTOR](http://www.konamiman.com/msx/msx-e.html#nextor)).

The host platform I used is MacOS but the Makefiles can be made compatible with other BSD and Linux based platforms too.

## Quickstart

```git clone git@gitlab.com:msx-projects/msx-devel-toolchain-for-macos.git```

```cd msx-devel-toolchain-for-macos/tools```

```./install.sh```

```cd ../examples/hello```

```make```

## Tools used

What you need:

* The SDCC -or Small Device C Compiler- from http://sdcc.sourceforge.net (version 3.6.0 included)
* Hex2bin for converting Intel hex format (.ihx) files into real binaries http://sourceforge.net/projects/hex2bin (version 2.3 included)
* A target MSX or emulator like CocoaMSX (MacOS, http://cocoamsx.com) or openMSX (http://openmsx.sourceforge.net) for testing your builds and/or a real MSX
* Libraries for MSX (SDCC Libraries from Konamiman http://www.konamiman.com/msx/msx-e.html and/or SDCC backend for MSXDOS from Avelino Herrera Morales http://msx.atlantes.org/index_en.html)
*  Xcode for easier building (tested with Xcode 8.2)

To deploy to a real MSX I use a small webserver (Mongoose, https://code.google.com/p/mongoose/) on the host platform and
on the MSX I use a Denyonet network card and the HGET tool (http://www.konamiman.com/msx/msx-e.html) to download the compiled binary onto the MSX.

## References

* Konamiman�s page http://www.konamiman.com/msx/msx-e.html
** SDCC libraries for MSX http://www.konamiman.com/msx/msx-e.html#sdcc
* Avelino Herrera Morales page http://msx.atlantes.org/index_en.html
