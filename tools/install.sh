#!/bin/sh

# installation script for MSX cross-platform development environment
# Maurice de Bijl 27 dec 2014

HTTP_GET_CLIENT_FOUND=0
HTTP_GET_CLIENT=""

PLATFORM=`uname -s`
# Darwin / CYGWIN_NT-5.1 / GNU/Linux

function downloadFile()
{
    URL=$1
    FILE=$2

    if [ "${HTTP_GET_CLIENT}" == "curl" ]; then
        `which curl` -o ${FILE} ${URL}

        if [ $? == 0 ]; then
            echo "Could not download ${URL}";
        fi

    fi

    if [ "${HTTP_GET_CLIENT}" == "wget" ]; then
        `which wget` -O ${FILE} ${URL}

        if [ $? == 0 ]; then
            echo "Could not download ${URL}";
        fi

    fi
}



if [ ${HTTP_GET_CLIENT_FOUND} == 0 ]; then
    which curl

    if [ $? == 0 ]; then
        HTTP_GET_CLIENT_FOUND=1
        HTTP_GET_CLIENT="curl"
    fi
fi


if [ ${HTTP_GET_CLIENT_FOUND} == 0 ]; then
    which wget

    if [ $? == 0 ]; then
        HTTP_GET_CLIENT_FOUND=1
        HTTP_GET_CLIENT="wget"
    fi
fi


if [ ${HTTP_GET_CLIENT_FOUND} == 0 ]; then
  echo "No curl or wget found! Please be sure one of these tools is installed"
  exit 1
fi


#
# SDCC
#
#if [ "${PLATFORM}" == "Darwin" ]; then
#   downloadFile http://ftp3.ie.freebsd.org/pub/download.sourceforge.net/pub/sourceforge/s/sd/sdcc/sdcc-macosx/3.4.0/sdcc-3.4.0-universal-apple-macosx.tar.bz2 sdcc.tar.bz2
#fi

#if [ "${PLATFORM}" == "GNU/Linux" ]; then
#   downloadFile http://ftp3.ie.freebsd.org/pub/download.sourceforge.net/pub/sourceforge/s/sd/sdcc/sdcc-linux-x86/3.4.0/sdcc-3.4.0-i386-unknown-linux2.5.tar.bz2 sdcc.tar.bz2
#fi

# Download manually from: https://sourceforge.net/projects/sdcc/files/sdcc-macos-amd64/

tar -jxvf sdcc-3.9.0-x86_64-apple-macosx.tar.bz2
rm -rf sdcc
mv sdcc-3.9.0 sdcc

#
# Hex2Bin
#
#downloadFile http://ftp3.ie.freebsd.org/pub/download.sourceforge.net/pub/sourceforge/h/he/hex2bin/hex2bin/Hex2bin-1.0.12.tar.bz2 hex2bin.tar.bz2
tar -jxvf Hex2bin-2.3.tar.bz2
rm -rf hex2bin
mv Hex2bin-2.3 hex2bin
cd hex2bin
rm hex2bin
make clean
make
cd ..
